{-# LANGUAGE BangPatterns #-}
module Main where

import Stochastic.Generator
import Stochastic.Uniform
import Stochastic.Distributions
import System.Environment
import System.IO
import Data.IORef

main :: IO ()
main = do
  [count] <- getArgs
  let n  = read count :: Integer
  let g  = xorshift128plus 42
  let g' = dropGen (next) n g
  iog <- liftGen (next) g'
  x <- nextIO iog
  putStrLn (show x)
--  ref <- newIORef g
--  invert $ step n (f ref)
  return ()


myUncons (x:xs) = (x, xs)

step :: Integer -> IO Int -> [IO Int]
step 0 _  = []
step n io = (io:(step (n-1) io))

invert :: [IO a] -> IO [a]
invert [] = return []
invert (io:ios) = do
  x <- io
  xs <- invert ios
  return (x:xs)

f :: IORef UniformRandom -> IO Int
f ref = do
  g <- readIORef ref
  let (x, g') = next g
  writeIORef ref g'
  return x

