import Stochastic.Generator
import Stochastic.Distribution.Continuous
import Stochastic.Distributions
import Stochastic.Distributions.Continuous
import Stochastic.Generators.Continuous
import System.Directory
import System.IO

dumpSamples :: (String, [Double]) -> IO ()
dumpSamples  (gen_name, samples) = do
  _ <- createDirectoryIfMissing True "dist/samples"
  _ <- withFile
       ("dist/samples/" ++ gen_name ++ ".dat")
       WriteMode
       (\handle -> mapM_
                   (hPrint handle)
                   samples
       )
  return ()

cds seed sample_size = [
  ("uniform",          fst $ rands sample_size $ (stdBase seed)    )
  ,("exponential",     fst $ rands sample_size $ mkExp     1   (stdBase seed))
  ,("normal",          fst $ rands sample_size $ mkNormal  0 1 (stdBase seed) )
  ]

main :: IO ()
main =
  let gens = cds 42 100000 in
  mapM_ (dumpSamples) gens 
