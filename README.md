### Stochastic.Distribution
    Provides the common type classes ContinuousDistribution & DiscreteDistribution

### Stochastic.Distributions 
    Provides convenience construction tools s.t. any Uniform RNG can be used to produce the supported variates

### Example use:

```Haskell
import Stochastic.Distributions

main :: IO ()
main = 
  -- constuct Exp with seed = 42, slope = 1, discard the rng
  let exp         = mkExp stdDistributions 42 1 in
  let (rs, exp')  = randDoubles 10 exp in
  do
    mapM (\d -> putStrLn $ show d) rs
    return ()
```