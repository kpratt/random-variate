module Stochastic.Distribution.Continuous where

class ContinuousDistribution g where
  cdf  :: g -> Double -> Double
  cdf' :: g -> Double -> Double
  
  pdf  :: g -> Double -> Double -> Double
  pdf g a b = (cdf g b) - (cdf g a)

  degreesOfFreedom :: g -> Int
  
