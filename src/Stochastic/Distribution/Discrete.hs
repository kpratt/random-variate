module Stochastic.Distribution.Discrete(
  module Stochastic.Generators.Discrete
  ,DiscreteDistribution(..)
) where

import Stochastic.Generators.Discrete

class DiscreteDistribution g where
  cdf  :: g -> Int -> Double
  cdf' :: g -> Double -> Int
  pmf  :: g -> Int -> Double

