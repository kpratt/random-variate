module Stochastic.Generators.Continuous where

import Stochastic.Tools

class ContinuousSample g where
  rand  :: g -> (Double, g)
  entropy :: g -> Int
  
  rands :: Int -> g -> ([Double], g)
  rands n g0 = statefully (rand) n g0

  toList :: g -> [Double]
  toList g = fst $ rands (entropy g) g
