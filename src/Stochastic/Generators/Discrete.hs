module Stochastic.Generators.Discrete where

import Stochastic.Tools

class DiscreteSample g where
  rand :: g -> (Int, g)
  entropy :: g -> Int

  rands :: Int -> g -> ([Int], g)
  rands n g0 = statefully (rand) n g0

  toList :: g -> [Int]
  toList g = fst $ rands (entropy g) g
  
  
